import React, { useState, useEffect } from "react";
import List from "../List";
import Badge from "../Badge";

import closeSvg from "../../assets/img/close.svg";
import "./index.scss";
import axios from "axios";

const AddList = ({ colors, onAdd }) => {
  const [showPopup, setShowPopup] = useState(false);
  const [selectedColor, setSelectedColor] = useState(1);
  const [inputValue, setInputValue] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (Array.isArray(colors)) {
      setSelectedColor(colors[0].id);
    }
  }, [colors]);

  const onClose = () => {
    setInputValue("");
    setShowPopup(false);
    setSelectedColor(colors[0].id);
  };

  const addList = () => {
    if (!inputValue) {
      alert("Введіть назву папки!");
      return;
    }
    setIsLoading(true);
    axios
      .post("http://localhost:3001/lists", {
        name: inputValue,
        colorId: selectedColor
      })
      .then(({ data }) => {
        const color = colors.filter(c => c.id === selectedColor)[0];
        const listObj = { ...data, color, tasks: [] };
        onAdd(listObj);
        onClose();
      })
      .catch(() => {
        alert("Помилка при додавання списку!");
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  return (
    <div className="add-list">
      <List
        onClick={() => setShowPopup(true)}
        items={[
          {
            className: "list__add-btn",
            icon: (
              <svg
                width="12"
                height="12"
                viewBox="0 0 12 12"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M6 1V11"
                  stroke="#868686"
                  strokeWidth="1.5"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
                <path
                  d="M1 6H11"
                  stroke="#868686"
                  strokeWidth="1.5"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
              </svg>
            ),
            name: "Додати список"
          }
        ]}
      />
      {showPopup && (
        <div className="add-list__popup">
          <button className="add-list__popup-close" onClick={onClose}>
            <img src={closeSvg} alt="close-button" />
          </button>
          <input
            className="field"
            onChange={event => setInputValue(event.target.value)}
            value={inputValue}
            type="text"
            placeholder="Название папки"
          />
          <div className="add-list__popup-colors">
            {colors.map(color => (
              <Badge
                key={color.id}
                onClick={() => setSelectedColor(color.id)}
                color={color.name}
                className={selectedColor === color.id && "active"}
              />
            ))}
          </div>
          <button onClick={addList} className="button">
            {isLoading ? "Дадавання..." : "Додати"}
          </button>
        </div>
      )}
    </div>
  );
};

export default AddList;
