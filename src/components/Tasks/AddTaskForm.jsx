import React, { useState } from "react";
import axios from "axios";
import addSvg from "../../assets/img/add.svg";

const AddTaskForm = ({ list, onAddTask }) => {
  const [visibleForm, setVisibleForm] = useState(false);
  const [inputValue, setInputValue] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const toggleFormVisible = () => {
    setVisibleForm(!visibleForm);
    setInputValue("");
  };

  const addTask = () => {
    const obj = {
      listId: list.id,
      text: inputValue,
      completed: false
    };
    setInputValue(true);
    axios
      .post("http://localhost:3001/tasks", obj)
      .then(({ data }) => {
        onAddTask(list.id, data);
        toggleFormVisible();
      })
      .catch(() => {
        alert("Помилка при додавання задачі!");
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  return (
    <div className="tasks__form">
      {!visibleForm ? (
        <div className="tasks__form-new" onClick={toggleFormVisible}>
          <img src={addSvg} alt="add icon" />
          <span>Нова задача</span>
        </div>
      ) : (
        <div className="tasks__form-block">
          <input
            type="text"
            value={inputValue}
            onChange={event => {
              setInputValue(event.target.value);
            }}
            className="field"
            placeholder="Текст задачи"
          />
          <button disabled={isLoading} onClick={addTask} className="button">
            {isLoading ? "Додавання" : "Додати задачу"}
          </button>
          <button onClick={toggleFormVisible} className="button button--gray">
            Відмінити
          </button>
        </div>
      )}
    </div>
  );
};

export default AddTaskForm;
