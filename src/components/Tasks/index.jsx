import React from "react";
import axios from "axios";
import { Link } from "react-router-dom";

import AddTaskForm from "./AddTaskForm";
import Task from "./Task";
import editSvg from "../../assets/img/edit.svg";

import "./index.scss";

const Tasks = ({
  list,
  onEditTitle,
  onAddTask,
  withoutEmpty,
  onRemoveTask,
  onEditTask,
  onCompleteTask
}) => {
  const editTitle = () => {
    const newTitle = window.prompt("Назва списку", list.name);
    if (newTitle) {
      onEditTitle(list.id, newTitle);
      axios
        .patch("http://localhost:3001/lists/" + list.id, {
          name: newTitle
        })
        .catch(() => {
          alert("Не вдалося обновити ім'я списку");
        });
    }
  };

  return (
    <div className="tasks">
      <Link
        to={`/lists/${list.id}/`}
        className="tasks__title"
        style={{ color: list.color.hex }}
      >
        {list.name}
        <img onClick={editTitle} src={editSvg} alt="edit icon" />
      </Link>
      <div className="tasks__item">
        {!withoutEmpty && list.tasks && !list.tasks.length && (
          <h3>Задачи отсутствуют</h3>
        )}
        {list.tasks &&
          list.tasks.map(task => (
            <Task
              key={task.id}
              {...task}
              list={list}
              onEdit={onEditTask}
              onRemove={onRemoveTask}
              onComplete={onCompleteTask}
            />
          ))}
        <AddTaskForm key={list.id} list={list} onAddTask={onAddTask} />
      </div>
    </div>
  );
};

export default Tasks;
