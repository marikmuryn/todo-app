import React from "react";

import removedSvg from "../../assets/img/removed.svg";
import editSvg from "../../assets/img/edit.svg";

const Task = ({ id, text, completed, list, onRemove, onEdit, onComplete }) => {
  const onChangeCheckbox = e => {
    onComplete(list.id, id, e.target.checked);
  };

  return (
    <div key={id} className="tasks__item_checkbox">
      <label onChange={onChangeCheckbox}>
        <input type="checkbox" checked={completed} />
        <i />
      </label>
      <p className="tasks__value">{text}</p>
      <div className="tasks__button">
        <button
          onClick={() => onEdit(list.id, { id, text })}
          type="button"
          className="tasks__button-edit"
        >
          <img src={editSvg} alt="edit icon" />
        </button>
        <button
          onClick={() => {
            onRemove(list.id, id);
          }}
          type="button"
          className="tasks__button-removed"
        >
          <img src={removedSvg} alt="removed icon" />
        </button>
      </div>
    </div>
  );
};

export default Task;
